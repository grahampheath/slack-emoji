const fs = require('fs');
let data = require('./emoji2020-10-27.json');
let pages = data.map((data) => {
  return data.emoji.map((e) => {
    console.log(Object.keys(e), e['user_display_name']);
    if (e.user_display_name === 'Graham Heath') {
      console.log({e});
    }

    return {
      name: e.name,
      user: e.user_display_name,
      isAlias: e.is_alias ? e.alias_for : e.is_alias
    };
  });
});

let emoji = pages.reduce((a, page) => {
  a = [...a, ...page];

  return a
}, []);


let emojiByUser = {};
emoji.forEach((e) => {
  /*
  {
    name: '0percent',
    is_alias: 0,
    alias_for: null,
    url: 'https://emoji.slack-edge.com/T024FA276/0percent/61eb633978fad3f2.jpg',
    team_id: 'T024FA276',
    user_id: 'U07T36T6H',
    user_display_name: 'Hernan Garchtrom',
    avatar_hash: '360e16514189',
    can_delete: false,
    is_bad: false,
    synonyms: null
  }
  */
  if (!emojiByUser[e.user]) {
    emojiByUser[e.user] = {
      count: 0,
      count_alias: 0,
      items: [],
      items_alias: []
   };
  }

  if (e.isAlias) {
    emojiByUser[e.user].count_alias++;
    emojiByUser[e.user].items_alias.push(e.name);
  } else {
    emojiByUser[e.user].count++;
    emojiByUser[e.user].items.push(e.name);
  }
})

console.log('Graham Heath', emojiByUser['Graham Heath']);
let d = Object.keys(emojiByUser).map((e) => {
  let c = {user_name: e, ...emojiByUser[e]};
  return c
})
d = d.sort((a, b) => {
  return a.count - b.count;
})
d.forEach((e) => {
  console.log(`${e.count}: ${e.user_name}`);
})
fs.writeFile('./emoji2020-10-27out.json', JSON.stringify(d, null, 2) , 'utf-8');
