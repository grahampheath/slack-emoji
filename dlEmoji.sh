#!/usr/bin/env bash
d="$(date '+%Y-%m-%d')";
out="emoji$d.json";
echo "Output: $out"
echo "[" > "$out"
pages=47
for (( i = 1; i <= $pages; i++ )); do
  curl 'https://rga.slack.com/api/emoji.adminList?_x_id=b718020f-1603826760.787&slack_route=T024FA276&_x_version_ts=1603822035' \
    -H 'authority: rga.slack.com' \
    -H 'pragma: no-cache' \
    -H 'cache-control: no-cache' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.80 Safari/537.36' \
    -H 'dnt: 1' \
    -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary5IuCruXB3mxBysQK' \
    -H 'accept: */*' \
    -H 'origin: https://rga.slack.com' \
    -H 'sec-fetch-site: same-origin' \
    -H 'sec-fetch-mode: cors' \
    -H 'sec-fetch-dest: empty' \
    -H 'accept-language: en-US,en;q=0.9' \
    -H 'cookie: b=.avn2qna3bxuujrn1wjsderypl; OptanonConsent=isIABGlobal=false&datestamp=Wed+Oct+07+2020+15%3A49%3A56+GMT-0500+(Central+Daylight+Time)&version=6.7.0&hosts=&consentId=e8a1081c-aa67-4d7f-9651-99f2e48307a7&interactionCount=1&landingPath=https%3A%2F%2Fslack.com%2Fblog%2Ftransformation%2Fremote-employee-experience-index-launch&groups=C0004%3A0%2CC0002%3A1%2CC0003%3A1%2CC0001%3A1; d=cDDmLM32hXbQIBkYPhrv6CL4S4qvN2M5uBGdqCpk6BM2v5FqoMXVnzyDYq0rEUp0S5VuOIPDFRa9g4d0vU92KpiR%2BDbXMlV34frpbWXRZ%2FqMp1C2EaNQapPmgWZrQ1VHulK%2F46%2FpbHxXP9WEpmhYzzrAWezDOeG%2FJ35tQSfMFusGzXPqY72D%2FA%3D%3D; d-s=1603815242; lc=1603815242; x=avn2qna3bxuujrn1wjsderypl.1603826747' \
    -H 'sec-gpc: 1' \
    --data-binary $'------WebKitFormBoundary5IuCruXB3mxBysQK\r\nContent-Disposition: form-data; name="page"\r\n\r\n'$i$'\r\n------WebKitFormBoundary5IuCruXB3mxBysQK\r\nContent-Disposition: form-data; name="count"\r\n\r\n100\r\n------WebKitFormBoundary5IuCruXB3mxBysQK\r\nContent-Disposition: form-data; name="token"\r\n\r\nxoxs-2151342244-4924640241-1457017155123-9a6205d33eb4960efd54e5267d6d171832233a2330908d424ac0c2490e41065c\r\n------WebKitFormBoundary5IuCruXB3mxBysQK\r\nContent-Disposition: form-data; name="_x_reason"\r\n\r\ncustomize-emoji-new-query\r\n------WebKitFormBoundary5IuCruXB3mxBysQK\r\nContent-Disposition: form-data; name="_x_mode"\r\n\r\nonline\r\n------WebKitFormBoundary5IuCruXB3mxBysQK--\r\n' \
    --compressed >> "$out"
  if [ "$i" -lt "$pages" ]; then
    echo "," >> "$out"
  fi
done
echo "]" >> "$out"
