## To download the json from slack:

Login to the website https://[workspace].slack.com/customize/emoji.

Inspect the network activity looking for an event like "/api/emoji.adminList".

Right click on the event -> Copy as cURL.

Update `dlEmoji.sh` in this folder with the copied cURL statement. This includes your authentication. You'll want to preserve the only modification made to this command:

    name="page"\r\n\r\n'$i$'\r\n

replaces

    name="page"\r\n\r\n'1'\r\n

{this step needs improvement, sorry}. Now go back into the customize/emoji page in your browser and scroll the list until scrolling doesn't add to the list. Look up the last page number in the last request for "/api/emoji.adminList". Now change the `pages` total in `dlEmoji.sh` to match.

Running the dlEmoji script should produce a file called `emoji[todays date].json`.

Prune any empty entries off the end of your new json, as well as ensure there arent any 404/500 errors.
