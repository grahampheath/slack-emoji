const emojis = require('./emoji.json');
const asyncPool = require('tiny-async-pool');
const concurrency = 10;
const util = require('util');
const path = require('path')

const exec = util.promisify(require('child_process').exec);

let emojiMap = [];
emojis.forEach((emojiPage) => {
  emojiPage.emoji.forEach((e) => {
    emojiMap.push(e);
  });
});
let count = 1;
asyncPool(concurrency, emojiMap, async (emoji) => {
  console.log(`${count++}${emojiMap.length}`);
  let fileType = path.extname(emoji.url);
  await exec(`curl -o ./emojiScrape/${emoji.name}${fileType} ${emoji.url}`);
})
console.log('done');
